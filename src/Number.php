<?php declare(strict_types=1);

use JetBrains\PhpStorm\Pure;

class Number
{
    public function __construct(private int $value) {}

    public function isMultipleOf(int $num): bool
    {
        return ($this->value % $num === 0);
    }

    #[Pure] public function __toString(): string
    {
        if ($this->isMultipleOf(5) && $this->isMultipleOf(3)) {
            return "FizzBuzz";
        } elseif ($this->isMultipleOf(5)) {
            return "Fizz";
        } elseif ($this->isMultipleOf(3)) {
            return "Buzz";
        } else {
            return "{$this->value}";
        }
    }
}
