<?php declare(strict_types=1);

spl_autoload_register(function ($className) {
    include './src/' . $className . '.php';
});

use PHPUnit\Framework\TestCase;

final class NumberTest extends TestCase
{
    public function __construct(private int $start = 1, private int $end = 100) {
        parent::__construct();
    }

    public function testExecutor(): void
    {
        for ($i=$this->start; $i<=$this->end; $i++) {
            $num = new Number($i);
            echo "$num\n";

            if ($num->isMultipleOf(3) && $num->isMultipleOf(5)) {
                $this->assertEquals("FizzBuzz", $num);
            } elseif ($num->isMultipleOf(5)) {
                $this->assertEquals('Fizz', $num);
            } elseif ($num->isMultipleOf(3)) {
                $this->assertEquals('Buzz', $num);
            } else {
                $this->assertEquals("{$i}", $num);
            }
        }
    }
}